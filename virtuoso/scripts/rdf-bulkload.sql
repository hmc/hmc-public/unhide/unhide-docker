--  Add all files that end in .jsonld and .ttl
--
ld_dir_all ('data', '*.jsonld', 'http://purls.helmholtz-metadaten.de/unhidekg');
ld_dir_all ('data', '*.ttl', 'http://purls.helmholtz-metadaten.de/unhidekg');
--
--  Add all files that end with .gz or .xz to show that the Virtuoso bulkloader 
--  can load compressed files without manual decompression
--
ld_dir_all ('data', '*.gz', 'http://purls.helmholtz-metadaten.de/unhidekg');

ld_dir_all ('data', '*.xz', 'http://purls.helmholtz-metadaten.de/unhidekg');
--
--  Now load all of the files found above into the database
--
rdf_loader_run();

-- save the data into the database
checkpoint;
--
--  End of script
--
