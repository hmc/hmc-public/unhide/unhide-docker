# Building up Virtuoso universal server

 
The virtuoso service as a part of the [docker compose of unHIDE](https://codebase.helmholtz.cloud/hmc/hmc-public/unhide/unhide-docker/-/blob/main/docker-compose.yml?ref_type=heads) performs the following steps:

  1. Creates an instance of the Virtuoso universal server.
  2. Mounts the local `data` directory on the host os, as `/database/data` in the docker instance.
  3. Mounts the local `scripts` directory on the host OS, as `/initdb.d` in the docker instance.
  4. Sets the `dba` password to a trivial password (it is highly recommended to change it after the installation either from the conductor or by launching isql).
  5. Exposes the standard network ports `1111` and `8890`.

Once Virtuoso has started, you can use a browser to connect to the local SPARQL endpoint via:

```text
http://localhost:8890/sparql
```
  

### The ./data directory

This directory contains the initial data that we want to load into the database. 

```
./database
├── juelichdata.ttl
├── software_rodare.ttl
```
The `docker-compose.yml` script mounts this data directory below the `/database` directory as `/database/data`.

Since the database directory is in the `DirsAllowed` setting in the `[Parameters]` section,
we do not have to make modifications to the `virtuoso.ini` configuration file.


#### Notes on the bulkloader
The Virtuoso bulkloader can automatically handle compressed (.gz, .bz2 and .xz) files and will choose the appropriate decompression function to read the content from the file.
It then chooses an appropriate parser for the data based on the suffix of the file:

  * **N-QUAD** when using the .nq or .n4 suffix
  * **Trig** when using the .trig suffix
  * **RDF/XML** when using .xml, .owl, .rdf or .rdfs suffix
  * **Turtle** when using .ttl or .nt suffix

While the function <code>ld_dir_all()</code> allows the operator to provide a graph name, it is much simpler
for the data directory to contain hints on the graph names to use, especially when there are
a number of files that needed to be loaded either in the same graph or in different graphs.
If the `global.graph` file is not present, the graph argument of the <code>ld_dir_all()</code>
function is used.




### The ./scripts directory

This directory can contain a mix of shell (.sh) scripts and Virtuso PL (.sql) scripts that can perform functions such as:

  * Bulkloading data into the virtuoso database.
  * Adding new virtuoso users.
  * Granting permissions to virtuoso users.

For security purposes, Virtuoso will run the `.sql` scripts in a special mode and will not
respond to connections on its SQL (1111) and/or HTTP (8890) ports.

At the end of each `.sql` script, Virtuoso automatically performs a `checkpoint` to make sure
the changes are fully written back to the database. This is very important for scripts
that use the bulkloader function `rdf_loader_run()` or for any reason manually change the ACID
mode of the database.



