# unhide-docker

This repository contains docker files to install and deploy all software componets of UnHIDE in its fullness.

It includes:

- unhide-ui
- data-harvesting, including the indexer and solr
- an Apache Jena instance
- nginx-proxy as a web proxy to acess the webseite and the api
- web front end
- data pipeline

The main and the dev branch of this repository point always to the last commits of the main and dev branches of the other
unhide repositories.

## Installation

### Prerequisites

The description here is for a Ubuntu Linux machine.

- Git
- docker-compose
- set up data volumes

#### Install git

```
sudo apt install git
```

#### Install docker and docker-compose

On a deployment machine, you probably want the data from docker of the containers and docker volumes saved under a special path.
For example a mounted volume which is backed up.
For this you need to set ```data-root``` in ```config /etc/docker/daemon.json```:
```
{"data-root": "/data/docker"}
```

To install docker see https://phoenixnap.com/kb/install-docker-on-ubuntu-20-04.

```
sudo apt remove docker docker-engine docker.io containerd runc
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-cache policy docker-ce
sudo apt install docker-ce -y
```
Then install docker-compose accordingly.

## Checkout this code repository

Since this repository just contains the docker-files and links. you need to do a:

```
git clone --recurse-submodules git@codebase.helmholtz.cloud:hmc/hmc-public/unhide/unhide-docker.git
```

## Configuration

You can either run ```make``` to create the needed `.env` file or created it manually.
The `.env` file contains the Hostname, which is for development on a local machine `localhost`
and for deployment the domain.

## Start up

On first start up all containers have to be build, for this run
```
docker-compose up -d --build
```
and the solr database needs to be initialized either on disk or on a docker volume.
```
docker-compose run -u root solr chown solr:solr /var/solr/data/unhide/data
```

For all needs start ups one can bring everything back online via:
```
docker-compose up -d
```

## Tear down and clean up
To shut down all running containers you can execute
```
docker-compose down
```

If you want to clean up docker the following commands are useful
```
docker image prune -a
docker volume prune -a
```

## License

The software is distributed under the terms and conditions of the MIT license which is specified in the `LICENSE` file.

## Acknowledgement

This project was supported by the Helmholtz Metadata Collaboration (HMC), an incubator-platform of the Helmholtz Association within the framework of the Information and Data Science strategic initiative.

